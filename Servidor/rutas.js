import express from "express";
import controlador from "./controladores.js";
const rutaDatos = express.Router();

rutaDatos.post("/informacion", controlador.obtenerDatos);

export default rutaDatos;

