import express from "express";
import cors from "cors";
import morgan from "morgan";

const app = express();

import rutaDatos from "./rutas.js";

app.set("port", process.env.PORT || 3000);
app.use(morgan("dev"));
app.use(cors());
app.use(express.json());

app.use("/", rutaDatos);

app.listen(app.get("port"), () => {
  console.log("Servidor en 3000");
});
