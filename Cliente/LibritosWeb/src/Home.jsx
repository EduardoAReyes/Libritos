import React from "react";
import Caratula from "./Caratula.jsx";
import Barra from "./Barra";
import Footer from "./footer.jsx";

import "./style/home.css";
function Home() {
  return (
    <div className="casa">
      <Barra />
      <div className="contenido">
        <Caratula
          obra="Eso"
          caratula="ESO_CARATULA.png"
          autor="Stephen King"
          sinopsis="Eso no es payaso, eso es algo que nadie puede explicar, eso es... horrible."
        />
        <Caratula
          obra="Misery"
          sinopsis="Un escritor es secuestrado por su mayor fan, y esta le hace vivir el verdadero infierno."
          caratula="MISERY_CARATULA.png"
          autor="Stephen King"
        />
        <Caratula
          obra="Carrie"
          caratula="CARRIE.png"
          autor="Stephen King"
          sinopsis="Una pobre niña con poderes psiquicos es capaz de provocar el mayor de los horrores"
        />
        <Caratula
          sinopsis="Libro de terror donde un simple hotel es el verdadero villano"
          obra="El Resplandor"
          caratula="RESPLAN_CARATULA.png"
          autor="Stephen King"
        />
        <Caratula
          obra="HP La piedra filosofal"
          sinopsis="Libro de la exitosa serie de libros Harry Poter, el niño mago."
          caratula="HP_PiedraFilosofal.png"
          autor="JK Rowling"
        />
        <Caratula
          sinopsis="Libro de la exitosa serie de libros Harry Poter, el niño mago."
          obra="HP La camara secreta"
          caratula="HP_CamaraSecreta.png"
          autor="JK Rowling"
        />
        <Caratula
          sinopsis="Libro de la exitosa serie de libros Harry Poter, el niño mago."
          obra="HP El prisionero de Azkhaban"
          caratula="HP_PrisioneroAzkhaban.png"
          autor="JK Rowling"
        />
        <Caratula
          sinopsis="Libro de la exitosa serie de libros Harry Poter, el niño mago."
          obra="HP El caliz de fuego"
          caratula="HP_CalizFuego.png"
          autor="JK Rowling"
        />
        <Caratula
          sinopsis="Libro de la exitosa serie de libros Harry Poter, el niño mago."
          obra="HP La orden del fenix"
          caratula="HP_OrdenFenix.png"
          autor="JK Rowling"
        />
        <Caratula
          sinopsis="Libro de la exitosa serie de libros Harry Poter, el niño mago."
          obra="HP El misterio del principe"
          caratula="HP_Principe.png"
          autor="JK Rowling"
        />
        <Caratula
          sinopsis="Libro de la exitosa serie de libros Harry Poter, el niño mago."
          obra="HP Las reliquias de la muerte"
          caratula="HP_Reliquias.png"
          autor="JK Rowling"
        />
        <Caratula obra="Yo,Robot" caratula="yoRobot.png" autor="Isaac Asimov" />
        <Caratula
          obra="Perdida"
          caratula="perdida.png"
          autor="Gillian Flynn"
          sinopsis="Triller psicologico que te hara dudar quien es el villano."
        />
        <Caratula
          obra="La Milla Verde"
          caratula="MILLA_VERDE.png"
          autor="Stephen King"
          sinopsis="John Coffe como el cafe es la causa de una serie de meligros inexplicables"
        />
        <Caratula
          obra="1984"
          caratula="1984.png"
          autor="George Orwell"
          sinopsis="Distopia moderna causada por un partido politico que logra cambiar el pensamiento de sus pobladores"
        />
        <Caratula
          obra="Un Mundo Feliz"
          caratula="UnMundoFeliz.png"
          autor="Aldoue Huxly"
          sinopsis="Una distopia futurista donde los habitantes de este mundo nunca dejan de ser felices."
        />
        <Caratula
          obra="Fahrenheit 451"
          caratula="fahrenheit.png"
          autor="Ray Bradbury"
          sinopsis="Fahrenheit 451 es la temperatura a la que el papel se quema, ya que en este mundo, los libros estan totalmente prohibidos."
        />
        <Caratula
          obra="El Cartero"
          caratula="Cartero.png"
          autor="Charles Bukowski"
          sinopsis="cuenta las anecdotas del autor en los tiempos en los que trabajaba de cartero"
        />
        <Caratula
          obra="Cementerio de Animales"
          caratula="CEMENTERIO.png"
          autor="Stephen King"
          sinopsis="Libro de terror que trata sobre una persona que pierde a su hijo y decide revivirlo con ayuda de un cementerio indio."
        />
        <Caratula
          obra="Animales fantasticos y donde encontrarlos"
          caratula="AnimalesFantasticos.png"
          autor="JK Rowling"
          sinopsis="Cuenta acerca de criaturas fantasticas y donde poder encontrarlas por jk rowling."
        />
      </div>
      <Footer />
    </div>
  );
}

export default Home;
