import React from "react";
import "./Input.css";

function Input({ placeholder, type, Introducir }) {
  return (
    <input
      type={type}
      placeholder={placeholder}
      required
      onChange={(e) => {
        Introducir(e.target.value);
      }}
    />
  );
}

export default Input;
