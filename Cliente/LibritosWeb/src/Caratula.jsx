import React from "react";
import "./style/caratula.css";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";

function Caratula({ obra, caratula, autor, sinopsis }) {
  const navegar = useNavigate();

  return (
    <article
      style={{ backgroundImage: `url(${caratula})` }}
      className="imagen"
      onClick={(e) => {
        navegar("/LibroInfo", {
          state: {
            obra: obra,
            caratula: caratula,
            autor: autor,
            sinopsis: sinopsis,
          },
        });
      }}
    >
      <div className="descrip">
        {obra}
        <p className="autor">
          Autor: <br /> {autor}
        </p>
      </div>
    </article>
  );
}

export default Caratula;
