import React from "react";
import "./style/footer.css";

function Footer() {
  return (
    <footer class="pie-pagina dark:bg-gray-900">
      <div class="grupo-1">
        <div class="box">
          <figure>
            <a href="#">
              {/* <img src="img/logotipo-sleedw.svg" alt="Logo de SLee Dw"> */}
              <img src="../public/Logo_Libritos.png" />
            </a>
          </figure>
        </div>
        <div class="box">
          <h2>SOBRE NOSOTROS</h2>
          <p>Grupo de personas que le encanta programar.</p>
          <p>Si te interesa nuestras vidas, buscanos aqui.</p>
        </div>
        <div class="box">
          <h2>SIGUENOS</h2>
          <div class="red-social">
            {/* Watsap jaja */}
            <a href="#" class="fa fa-facebook">
              <img src="bxl-facebook-square.svg" className="face-square" />
            </a>
            {/* <a href="#" class="fa fa-instagram"></a> */}
            {/* <a href="#" class="fa fa-twitter"></a> */}
            {/* <a href="#" class="fa fa-youtube"></a> */}
          </div>
        </div>
      </div>
      <div class="grupo-2 dark:bg-gray-950">
        <small>
          &copy; 2023 <b>Libritos</b> - Todos los Derechos Reservados.
        </small>
      </div>
    </footer>
  );
}

export default Footer;
