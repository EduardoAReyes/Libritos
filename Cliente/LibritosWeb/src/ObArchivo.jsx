import { useState } from "react";
import { Document, Page } from "react-pdf";
// import './style/archivoPDF.css'
import './style/pdf-div.css'
function ObArchivo({url}) {
  const [numPages, setNumPages] = useState();
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  return (
    <div className="pdf-div">
      {/* <p>
        Page {pageNumber} of {numPages}
      </p> */}
      <Document file={url} onLoadSuccess={onDocumentLoadSuccess}>
        {Array.apply(null, Array(numPages))
          .map((x, i) => i + 1)
          .map((page) => {
            return (
              <Page
                pageNumber={page}
                renderTextLayer={false}
                renderAnnotationLayer={false}
              />
            );
          })}
      </Document>
    </div>
  );
}

export default ObArchivo;
