import React from "react";
import { Outlet, Navigate } from "react-router-dom";

function Proteccion({ hijo }) {
  console.log("En proteccion" + hijo);
  let auth = localStorage.getItem("token");
  let auth2 = localStorage.getItem("token2");
  return auth ? <Outlet /> : <Navigate to="/" />;
}

export default Proteccion;
