import React from "react";
import Input from "./Input";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import "./style/login.css";

function Login({ estado }) {
  const navegar = useNavigate();
  const [Nombre, setNombre] = useState("");
  const [Contraseña, setContraseña] = useState("");
  const [autentificado, setAutentificado] = useState(false);

  useEffect(() => {
    window.addEventListener("beforeunload", antesDeCargar);

    return () => {
      window.removeEventListener("beforeunload", antesDeCargar);
    };
  }, []);

  const antesDeCargar = () => {
    localStorage.clear();
  };

  const login = () => {
    if (Nombre === "Lalo" && Contraseña === "Gato1234") {
      setAutentificado(true);
      // alert("Correcto: " + Nombre + " " + Contraseña);
      localStorage.setItem("token", autentificado);
      estado(autentificado);
      navegar("/Home");
    } else {
      alert("Contraseña incorrecta");
    }
  };

  return (
    <>
      <div className="login">
        <div className="formulario_cnt">
          <form action="#" className="formulario">
            <img src="Logo_Libritos_Azul.png" className="libro-logo"/>
            <h1 className="titulo_form font-sans ">Iniciar Sesión</h1>
            <Input placeholder="Nombre" Introducir={setNombre} />
            <Input
              placeholder="Contraseña"
              type="password"
              Introducir={setContraseña}
            />
            <button
              className="boton"
              onClick={(e) => {
                login();
              }}
            >
              Iniciar Sesión
            </button>
          </form>
        </div>
        <div className="img_libros"></div>
      </div>
    </>
  );
}

export default Login;
