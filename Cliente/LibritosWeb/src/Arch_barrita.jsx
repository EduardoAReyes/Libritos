import React from "react";
import "./style/arch_barrita.css";
import { useNavigate } from "react-router-dom";

function Arch_barrita() {
  const navegar = useNavigate();
  return (
    <div className="arch_barrita ">
      <img
        src="bx-left-arrow-alt.svg"
        className="imagen_arch"
        onClick={() => {
          navegar("/Home");
        }}
      />
    </div>
  );
}



export default Arch_barrita;
