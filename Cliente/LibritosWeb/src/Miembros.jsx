import React from "react";
import Barra from "./Barra";
import "./style/miembros.css";
import CardMiembro from "./CardMiembro";

function Miembros() {
  return (
    <>
      <Barra />
      <div className="miembros">
        <div className="w-full min-h-screen bg-blue-50 p-6">
          <h1 className="font-bold text-xl text-center md:text-3xl md:mt-12 mb-4">
            Miembros de equipo
          </h1>

          <div className="flex flex-wrap justify-center">
            <CardMiembro nombre="Cisneros Almaraz Mariel" foto="mariel.png" correo="20680067@cuautla.tecnm.mx"/>
            <CardMiembro nombre="Pavon Sanchez David Misael" foto="David.png" correo="20680154@cuautla.tecnm.mx" />
            <CardMiembro
              nombre="Alvares Gonzalez Martha Betsabe"
              foto="betsa.png"
              correo="20680041@cuautla.tecnm.mx"
            />
            <CardMiembro nombre="Lopez Espitia Lizeth" foto="liz.png" correo="20680117@cuautla.tecnm.mx"/>
            <CardMiembro
              nombre="Aguilar Yañez Jennifer Ariana"
              foto="Ari.png"
              correo="20680036@cuautla.tecnm.mx"
            />
            <CardMiembro nombre="Reyes Rosales Eduardo Alberto" foto="YO.png" correo="20680170@cuautla.tecnm.mx"/>
          </div>
        </div>
      </div>
    </>
  );
}

export default Miembros;
