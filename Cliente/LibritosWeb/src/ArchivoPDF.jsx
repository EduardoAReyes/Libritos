import React from "react";
// import "./style/archivo.css";

import "./style/pdf.css";
import { useLocation } from "react-router-dom";
import Arch_barrita from "./Arch_barrita";
import ObArchivo from "./ObArchivo";
import { pdfjs } from "react-pdf";

pdfjs.GlobalWorkerOptions.workerSrc = new URL(
  "pdfjs-dist/build/pdf.worker.min.js",
  import.meta.url
).toString();

function ArchivoPDF() {
  const location = useLocation();
  let nombre = location.state.archivo;
  const nombreArch = `${nombre}.pdf`;

  // console.log(nombreArch);
  return (
    <>
      <Arch_barrita />
      <div className="archivo_cont">
        {/* <img src="./books/paginas/44513_AAVV-El relato del horror 1 primer capitulo_page-0001.jpg" alt="" /> */}
        {/* <embed src={nombreArch} type="" className="arch" /> */}
        <ObArchivo url={nombreArch}/>
      </div>
    </>
  );
}

export default ArchivoPDF;
