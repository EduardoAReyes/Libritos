import React from "react";
import "./style/app.css";
import Login from "./Login";
import Home from "./Home";
import Libro_Info from "./Libro_Info";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState } from "react";
import Proteccion from "./Proteccion";
import Archivo from "./ArchivoPDF";
import Contacto from "./Contacto";
import Miembros from "./Miembros";

function App() {
  const [logueado, setLogueado] = useState(false);

  const estado = (validado) => {
    setLogueado(validado);
    console.log("En app" + validado);
  };

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login estado={estado} />} />
          <Route element={<Proteccion hijo={logueado} />}>
            <Route path="/Home" element={<Home />} />
            <Route path="/LibroInfo" element={<Libro_Info />} />
            <Route path="/Archivo" element={<Archivo />} />
            <Route path="/Contacto" element={<Contacto />} />
            <Route path="/Miembros" element={<Miembros />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
