import React from "react";
import Barra from "./Barra";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import Axios from "axios";

// import "./style/Libro_Info.css";
function Libro_Info() {
  const navegar = useNavigate();
  const urlDatos = "http://localhost:3000/informacion";
  const location = useLocation();
  let dato = location.state.obra;
  let img = location.state.caratula;
  let aut = location.state.autor;
  let sinop = location.state.sinopsis;

  // const [sinopsis, setSinopsis] = useState("");

  // const consulta = async () => {
  //   // const resp = await fetch(urlDatos);
  //   // const res = await resp.json();
  //   Axios.post(urlDatos, {
  //     obra: dato,
  //   })
  //     .then((res) => {
  //       console.log(res);
  //       console.log(res.data[0].sinopsis);
  //       setSinopsis(res.data[0].sinopsis);
  //     })
  //     .catch((err) => {
  //       console.error(err);
  //     });
  // };

  // useEffect(() => {
  //   consulta();
  // }, []);

  return (
    // <div className="libro_info">
    //   <Barra />
    //   <div className="libro_cont">
    //     <div className="datos">
    //       <h1 className="titulo_libro">{dato}</h1>
    //       <h2>Sinopsis:</h2>
    //       <p >{sinop}</p>
    //       <h2>Autor:</h2>
    //       <p>{aut}</p>

    //       <button
    //         className="btn_visualizar"
    //         onClick={() => {
    //           navegar("/Archivo", {
    //             state: {
    //               archivo: dato,
    //             },
    //           });
    //         }}
    //       >
    //         Visualizar
    //       </button>
    //     </div>
    //     <img src={img} className="caratula" />
    //   </div>
    // </div>
    <>
      <Barra />
      <div className="libro_info bg-gray-100 p-4 md:p-8 lg:p-12">
        <div className="libro_cont flex flex-col-reverse md:flex-row items-center">
          <div className="datos w-full md:w-1/2 lg:w-2/3">
            <h1 className="titulo_libro text-2xl md:text-3xl lg:text-4xl mb-4">
              {dato}
            </h1>
            <h2 className="text-xl md:text-2xl lg:text-3xl mb-2">Sinopsis:</h2>
            <p className="mb-4">{sinop}</p>
            <h2 className="text-xl md:text-2xl lg:text-3xl mb-2">Autor:</h2>
            <p className="mb-4">{aut}</p>

            <button
              className="btn_visualizar bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-700"
              onClick={() => {
                navegar("/Archivo", {
                  state: {
                    archivo: dato,
                  },
                });
              }}
            >
              Visualizar
            </button>
          </div>

          <img src={img} className="caratula w-full md:w-1/2 lg:w-1/3" />
        </div>
      </div>
    </>

  );
}

export default Libro_Info;
