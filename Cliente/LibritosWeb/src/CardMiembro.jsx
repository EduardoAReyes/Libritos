import React from "react";
import { useNavigate } from "react-router-dom";

function CardMiembro({ nombre, foto, correo }) {
    const navegar = useNavigate();
  return (
    <>
      <div className="flex flex-col bg-white rounded-lg shadow-md w-full m-6 overflow-hidden sm:w-52">
        <img src={foto} alt="" className="h-52 w-52" />

        <h2 className="text-center px-2 pb-5 h-12">{nombre}</h2>
        {/* <div className="contactar flex">
          <img src="bx-envelope-col.svg " className="w-6 " />
          <h2 className="text-center px-2 pb-5 h-12  text-sky-950 text-sm">
            {correo}
          </h2>
        </div> */}
        <div className="contactar flex items-center">
          <img src="bx-envelope-col.svg" className="w-6" alt="Ícono de sobre" />
          <h2 className="text-sky-950 text-xs ml-2">{correo}</h2>
        </div>

        <a
          href={"mailto:"+correo}
          onClick={()=>{
            navegar('/Miembros');
          }}
          className="dark:bg-gray-900 text-white p-3 text-center hover:dark:bg-gray-700 transition-all duration-500"
        >
          Contactar
        </a>
      </div>
    </>
  );
}

export default CardMiembro;
